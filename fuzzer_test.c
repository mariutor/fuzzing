#include "utility.h"
#include <stddef.h>
#include <stdint.h>

int LLVMFuzzerTestOneInput(char data[], size_t size) {
  char* temp = replace_string(data, size);
  free(temp);
  return 0;
}