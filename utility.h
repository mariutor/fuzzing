#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>


// Improvement would be to first find how many elements to be replaced and then create a more accurate sized char.

// gcc string.c -o string && ./string

char *replace_string(char str[], int length) {

    const int new_length = length * 6;

    char *new_string = malloc(new_length);

    int i = 0;
    int j = 0;

    while(str[i] != 0) {

        int t = str[i];
        char temp[] = {t};

        if (!strcmp("&", temp)) {
            new_string[j] = '&';
            new_string[j+1] = 'a';
            new_string[j+2] = 'm';
            new_string[j+3] = 'p';

            j += 4;


        }
        else if (!strcmp("<", temp)) {
            new_string[j] = '&';
            new_string[j+1] = 'l';
            new_string[j+2] = 't';

            j += 3;
        
        }
        else if (!strcmp(">", temp)) {
            new_string[j] = '&';
            new_string[j+1] = 'g';
            new_string[j+2] = 't';

            j += 3;
        }
        else {
            new_string[j] = (char) temp[0];
            j += 1;
        }

        i += 1;
    }

    return new_string;
}
