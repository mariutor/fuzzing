#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "utility.h"


int main() {

    char str[] = "test&<>test";

    size_t length = sizeof(str) / sizeof(str[0]);

    printf("Original string: %s \n", str);

    char *str2 =  replace_string(str, length);

    printf("Replaced string: %s \n ", str2);

    free(str2);
}

